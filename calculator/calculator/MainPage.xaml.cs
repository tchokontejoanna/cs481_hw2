﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace calculator
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage

    {
        string opt;
        private double nb1, nb2, r;
        bool first = false;
        bool second = false;

        public MainPage()
        {
            InitializeComponent();
            Clear(this, null);
        }

        private void SelectNumber(object sender, EventArgs e)

        {
            Button button = (Button)sender;

            if (result.Text.Contains(".") && button.Text == ".")
            {
                return;
            }

            if (result.Text == "0" && button.Text != ".")
                result.Text = button.Text;
            else
                result.Text += button.Text;


            if (!first)
            {
                nb1 = double.Parse(result.Text);
            }

            if (first && !second && opt != "")
            {
                nb2 = double.Parse(result.Text);
            }
        }

        private void SelectOperator(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (nb1.ToString() != "")
            {
                opt = button.Text;
                if (opt == "%")
                {
                    nb2 = 100;
                    first = true;

                    Calcul(sender, e);
                    return;
                }
                first = true;
            }
            result.Text = "0";

        }

        private void ChangeSign(object sender, EventArgs e)
        {
            double nb = double.Parse(result.Text);

            nb *= -1;
            result.Text = nb.ToString();
        }

        private void Clear(object sender, EventArgs e)
        {
            result.Text = "0";
            first = false;
            second = false;
            nb1 = 0;
            nb2 = 0;
            opt = "";
        }

        private void Calcul(object sender, EventArgs e)
        {

            if (nb2.ToString() != "")
            {
                second = true;
            }

            if (first && second)
            {
                switch (opt)
                {
                    case "+":
                        r = nb1 + nb2;
                        break;
                    case "x":
                        r = nb1 * nb2;
                        break;
                    case "-":
                        r = nb1 - nb2;
                        break;
                    case "/":
                        r = nb1 / nb2;
                        break;
                    case "%":
                        r = nb1 / nb2;
                        break;
                    default:
                        break;
                }

                result.Text = r.ToString();

            }
            opt = "";
            nb1 = r;
            first = true;
            second = false;

        }

    }
}
